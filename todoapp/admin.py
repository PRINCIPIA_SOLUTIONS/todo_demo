from django.contrib import admin

from .models import Todo


class TodoAdmin(admin.ModelAdmin):
    model = Todo
    list_display = [field.name for field in Todo._meta.fields if field.name != "id"]

admin.site.register(Todo,TodoAdmin)