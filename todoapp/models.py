from django.db import models

# Zavislost na aplikaci userapp :-)
from userapp.models import TodoUser


class Todo(models.Model):
    user = models.ForeignKey(TodoUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=256)
    body = models.TextField()
    todo_date = models.DateTimeField()

    def __str__(self):
        return self.title