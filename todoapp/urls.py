from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name = 'index'),
    url(r'^detail/(?P<id>\w{0,50})/$', views.detail, name = 'detail'),
    url(r'^add', views.add, name = 'add'),
    url(r'^edit/(?P<id>\w{0,50})$', views.edit, name = 'edit'),
    url(r'^edit/', views.edit, name = 'edit'),
    url(r'^delete/(?P<id>\w{0,50})/$', views.delete, name = 'delete'),
    url(r'^filter', views.filter, name = 'filter'),
    url(r'^user/(?P<id>\w{0,50})/$', views.user, name = 'user'),
]