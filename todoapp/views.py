from django.shortcuts import render, redirect
import datetime

from .models import Todo
from .models import TodoUser


def index(request):
    todos = Todo.objects.all().order_by("todo_date")

    context = {'todos': todos}

    return render(request, 'index.html', context)


def filter(request):
    todos = Todo.objects.all().order_by("todo_date").filter(
        todo_date__lte=datetime.datetime.now() + datetime.timedelta(days=7))

    context = {'todos': todos}

    return render(request, 'filter.html', context)


def detail(request, id):
    todo = Todo.objects.get(id=id)

    context = {'todo': todo}

    return render(request, 'detail.html', context)


def add(request):
    if (request.method == 'POST'):
        title = request.POST['title']
        body = request.POST['body']
        userName = request.POST['user']
        todoDate = request.POST['todo_date']

        todoUser = TodoUser.objects.get(username=userName)

        todo = Todo(title=title, body=body, user=todoUser, todo_date=todoDate)
        todo.save()

        return redirect('/todo')

    else:
        todoUsers = TodoUser.objects.all()
        context = {'todoUsers': todoUsers}
        return render(request, 'add.html', context)


def edit(request, id):
    if request.method == 'POST':
        todo = Todo.objects.get(id=request.POST['id'])
        todo.title = request.POST['title']
        todo.body = request.POST['body']
        todo.todoDate = request.POST['todo_date']
        todo.save()
        return redirect('/todo')

    else:
        todo = Todo.objects.get(id=id)
        context = {'todo': todo}
        return render(request, 'edit.html', context)


def delete(request, id):
    todo = Todo.objects.get(id=id)

    todo.delete()

    return redirect('/todo')



def user(request, id):

    todos = Todo.objects.all().filter(user_id=id)
    todoUser = TodoUser.objects.get(id=id)

    context = {'todos': todos,
               'user': todoUser}

    return render(request, 'user.html', context)