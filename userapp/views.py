from django.shortcuts import render, redirect


from .models import TodoUser


def index(request):
    users = TodoUser.objects.all()

    context = {'users': users}

    return render(request, 'uindex.html', context)

def detail(request, id):
    user = TodoUser.objects.get(id=id)

    context = {'user': user}

    return render(request, 'udetail.html', context)


def add(request):
    if (request.method == 'POST'):
        username = request.POST['username']
        firstName = request.POST['first_name']
        lastName = request.POST['last_name']
        email = request.POST['email']

        user = TodoUser(username=username,first_name=firstName,last_name=lastName,email=email)
        user.save()

        return redirect('/user')

    else:
        return render(request, 'uadd.html')
