from django.conf.urls import url

from userapp import views

urlpatterns = [
    url(r'^$', views.index, name = 'index'),
    url(r'^detail/(?P<id>\w{0,50})/$', views.detail, name='detail'),
    url(r'^add', views.add, name='add'),
]